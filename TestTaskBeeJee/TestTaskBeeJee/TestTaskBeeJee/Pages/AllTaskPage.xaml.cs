﻿using Newtonsoft.Json;
using Rg.Plugins.Popup.Services;
using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net.Http;
using TestTaskBeeJee.Models;
using TestTaskBeeJee.Services;
using TestTaskBeeJee.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace TestTaskBeeJee.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class AllTaskPage : ContentPage
    {
        ObservableCollection<Models.Task> allTask = new ObservableCollection<Models.Task>();
        ObservableCollection<PageNumberVM> listnumberpage = new ObservableCollection<PageNumberVM>();
        ObservableCollection<SortVM> sortField = new ObservableCollection<SortVM>() { new SortVM("Ім'я користувача", "username"), new SortVM("Email", "email"), new SortVM("Статус", "status") };
        ObservableCollection<SortVM> sortDir = new ObservableCollection<SortVM>() { new SortVM("А до Я", "asc"), new SortVM("Я до А", "desc") };
        int numbpage;
        int selnumbpage = 1;
        public AllTaskPage()
        {
            InitializeComponent();
            
        }
        protected override void OnAppearing()
        {
            base.OnAppearing();
            combofield.DataSource = null;
            combofield.DataSource = sortField;
            combofield.SelectedItem = sortField.FirstOrDefault(m => m.sortNameApi == Settings.SelectSortField);
            combodirection.DataSource = null;
            combodirection.DataSource = sortDir;
            combodirection.SelectedItem = sortDir.FirstOrDefault(m => m.sortNameApi == Settings.SelectSortDirection);
            GetTasks(1);
            if (string.IsNullOrEmpty(Settings.Token))
            {
                loginbtn.Text = "Увійти";
                editcolumn.IsHidden = true;
            }
            else
            {
                loginbtn.Text = "Вийти";
                editcolumn.IsHidden = false;
            }
            MessagingCenter.Subscribe<AddNewTaskPopup>(this, "RefreshData", (sender) =>
            {
                GetTasks(1);
            });
            MessagingCenter.Subscribe<EditTaskPopup>(this, "RefreshAdminData", (sender) =>
            {
                GetTasks(1);
            });
            MessagingCenter.Subscribe<LoginPopup>(this, "RefreshView", (sender) =>
            {
                loginbtn.Text = "Вийти";
                editcolumn.IsHidden = false;
            });
            MessagingCenter.Subscribe<EditTaskPopup>(this, "ErrorToken", (sender) =>
            {
                loginbtn.Text = "Увійти";
                Settings.Token = "";
                editcolumn.IsHidden = true;
            });
        }
        private async void GetTasks(int numberpage)
        {
            try
            {
                var client = new HttpClient();
                HttpResponseMessage responseMessage = await client.GetAsync($"https://uxcandy.com/~shapoval/test-task-backend/v2/?developer=AndriiShvets&page={numberpage}&sort_field={Settings.SelectSortField}&sort_direction={Settings.SelectSortDirection}");
                if (responseMessage.IsSuccessStatusCode)
                {
                    string json = await responseMessage.Content.ReadAsStringAsync();
                    var tasks = JsonConvert.DeserializeObject<Root>(json);
                    numbpage = Convert.ToInt32(tasks.message.total_task_count);
                    listnumberpage.Clear();
                    for (var i = 1; i <= (numbpage / 3) + 1; i++)
                    {
                        listnumberpage.Add(new PageNumberVM(i));
                    }
                    listpage.ItemsSource = null;
                    listpage.ItemsSource = listnumberpage;
                    listpage.SelectedItem = listnumberpage[numberpage-1];
                    allTask.Clear();
                    foreach (var task in tasks.message.tasks)
                    {
                        switch (task.status)
                        {
                            case 0:
                                task.statusText = "задача не выполнена";
                                break;
                            case 1:
                                task.statusText = "задача не выполнена, отредактирована админом";
                                break;
                            case 10:
                                task.statusText = "задача выполнена";
                                break;
                            case 11:
                                task.statusText = "задача отредактирована админом и выполнена";
                                break;
                        }
                        allTask.Add(task);
                    }
                    datagrid.ItemsSource = null;
                    datagrid.ItemsSource = allTask;

                }
                else
                {

                    var message = responseMessage.Content.ReadAsStringAsync().Result;
                    await DisplayAlert("All tasks", message, "OK");
                }
            }
            catch (Exception ex)
            {
            }
        }
        private async void newTask_Clicked(object sender, EventArgs e)
        {
            await PopupNavigation.Instance.PushAsync(new AddNewTaskPopup());
        }

        private void start_Clicked(object sender, EventArgs e)
        {
            GetTasks(1);
            selnumbpage = 1;
        }

        private void end_Clicked(object sender, EventArgs e)
        {
            GetTasks((numbpage / 3) + 1);
            selnumbpage = (numbpage / 3) + 1;
        }

        private void listpage_ItemTapped(object sender, Syncfusion.ListView.XForms.ItemTappedEventArgs e)
        {
            var model = e.ItemData as PageNumberVM;
            GetTasks(model.pageNumber);
            selnumbpage = model.pageNumber;
        }

        private void sortbtn_Clicked(object sender, EventArgs e)
        {
            Settings.SelectSortField = sortField[combofield.SelectedIndex].sortNameApi;
            Settings.SelectSortDirection = sortDir[combodirection.SelectedIndex].sortNameApi;
            GetTasks(selnumbpage);

        }

        private async void loginbtn_Clicked(object sender, EventArgs e)
        {
            if(string.IsNullOrEmpty(Settings.Token))
            await PopupNavigation.Instance.PushAsync(new LoginPopup());
        }

        private async void Button_Clicked(object sender, EventArgs e)
        {
            var bc = (Models.Task)(sender as Button)?.BindingContext;
            await PopupNavigation.Instance.PushAsync(new EditTaskPopup(bc.id, bc.text, bc.status));
        }
    }
}