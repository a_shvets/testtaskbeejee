﻿using Newtonsoft.Json;
using Rg.Plugins.Popup.Pages;
using Rg.Plugins.Popup.Services;
using System;
using System.Net.Http;
using TestTaskBeeJee.Models;
using TestTaskBeeJee.Services;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace TestTaskBeeJee.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class EditTaskPopup : PopupPage
    {
        int idtask;
        string texttask;
        public EditTaskPopup(int id, string text, int status)
        {
            InitializeComponent();
            idtask = id;
            texttask = text;
            textEntry.Text = text;
            if (status == 0 || status == 1) done.IsChecked = false;
            if (status == 10 || status == 11) done.IsChecked = true;
        }
        private void emailEntry_Unfocused(object sender, FocusEventArgs e)
        {
            if (string.IsNullOrEmpty(textEntry.Text)) textLayout.HasError = true;
        }
        private void emailEntry_Focused(object sender, FocusEventArgs e)
        {
            if (e.VisualElement.Equals(textEntry))
            {
                textLayout.HasError = false;
            }
        }

        private async void save_Clicked(object sender, EventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(textEntry.Text))
                {
                    textLayout.HasError = true;
                    return;
                }
                HttpClient httpClient = new HttpClient();
                MultipartFormDataContent form = new MultipartFormDataContent();
                string sendstatus = "";
                bool isCheck = done.IsChecked ?? false;
                if (textEntry.Text == texttask && !isCheck) sendstatus = "0";  
                if (textEntry.Text != texttask && !isCheck) sendstatus = "1";  
                if (textEntry.Text == texttask && isCheck) sendstatus = "10";  
                if (textEntry.Text != texttask && isCheck) sendstatus = "11";  
                form.Add(new StringContent(textEntry.Text), "text");
                form.Add(new StringContent(sendstatus), "status");
                form.Add(new StringContent(Settings.Token), "token");
                HttpResponseMessage response = await httpClient.PostAsync($"https://uxcandy.com/~shapoval/test-task-backend/v2/edit/{idtask}?developer=AndriiShvets", form);
                if (response.IsSuccessStatusCode)
                {
                    string json = await response.Content.ReadAsStringAsync();
                    var login = JsonConvert.DeserializeObject<Login>(json);
                    if (login.status == "error")
                    {
                        await DisplayAlert("Помилка", login.message.token, "Ok");
                        MessagingCenter.Send(this, "ErrorToken");
                        await PopupNavigation.Instance.PopAsync();
                        return;
                    }
                }
                MessagingCenter.Send(this, "RefreshAdminData");
                await PopupNavigation.Instance.PopAsync();
            }
            catch (Exception ex)
            {

            }
        }
    }
}