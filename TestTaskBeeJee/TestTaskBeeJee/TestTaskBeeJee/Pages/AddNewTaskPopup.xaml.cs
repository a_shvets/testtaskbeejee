﻿using Rg.Plugins.Popup.Pages;
using Rg.Plugins.Popup.Services;
using System;
using System.Net.Http;
using System.Text.RegularExpressions;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace TestTaskBeeJee.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class AddNewTaskPopup : PopupPage
    {
        Regex EmailRegex = new Regex(@"^(([^<>()[\]\\.,;:\s@\""]+(\.[^<>()[\]\\.,;:\s@\""]+)*)|(\"".+\""))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$");
        bool IsValidEmail = false;
        public AddNewTaskPopup()
        {
            InitializeComponent();
        }

        private async void save_Clicked(object sender, EventArgs e)
        {
            try
            {
                if(string.IsNullOrEmpty(emailEntry.Text) && string.IsNullOrEmpty(textEntry.Text) && string.IsNullOrEmpty(usernameEntry.Text))
                {
                    emailLayout.HasError = textLayout.HasError = usernameLayout.HasError = true;
                    return;
                }
                if (emailLayout.HasError || textLayout.HasError || usernameLayout.HasError) return;
                using (var formData = new MultipartFormDataContent())
                {
                    formData.Add(new StringContent(emailEntry.Text), "email");
                    formData.Add(new StringContent(textEntry.Text), "text");
                    formData.Add(new StringContent(usernameEntry.Text), "username");
                    using (HttpClient client = new HttpClient())
                    {
                        var responce = await client.PostAsync("https://uxcandy.com/~shapoval/test-task-backend/v2/create?developer=AndriiShvets", formData);
                        if (responce.IsSuccessStatusCode) await DisplayAlert("Успіх", "Завдання успішно додане", "Ок");
                    }

                }
                MessagingCenter.Send(this, "RefreshData");
                await PopupNavigation.Instance.PopAsync();

            }
            catch(Exception ex)
            {

            }
        }
        private void emailEntry_Unfocused(object sender, FocusEventArgs e)
        {
            IsValidEmail = string.IsNullOrEmpty(emailEntry.Text) ? false : EmailRegex.IsMatch(emailEntry.Text);
            if (!IsValidEmail) emailLayout.HasError = true;
            if (string.IsNullOrEmpty(textEntry.Text)) textLayout.HasError = true;
            if (string.IsNullOrEmpty(usernameEntry.Text)) usernameLayout.HasError = true;

        }

        private void emailEntry_Focused(object sender, FocusEventArgs e)
        {
            if (e.VisualElement.Equals(emailEntry))
            {
                emailLayout.HasError = false;
            }
            if (e.VisualElement.Equals(textEntry))
            {
                textLayout.HasError = false;
            } 
            if (e.VisualElement.Equals(usernameEntry))
            {
                usernameLayout.HasError = false;
            }
        }

    }
}