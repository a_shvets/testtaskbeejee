﻿using Newtonsoft.Json;
using Rg.Plugins.Popup.Pages;
using Rg.Plugins.Popup.Services;
using System;
using System.Net.Http;
using TestTaskBeeJee.Models;
using TestTaskBeeJee.Services;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace TestTaskBeeJee.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class LoginPopup : PopupPage
    {
        public LoginPopup()
        {
            InitializeComponent();
        }
        private void emailEntry_Unfocused(object sender, FocusEventArgs e)
        {
            if (string.IsNullOrEmpty(passwordEntry.Text)) passwordLayout.HasError = true;
            if (string.IsNullOrEmpty(usernameEntry.Text)) usernameLayout.HasError = true;
        }

        private void emailEntry_Focused(object sender, FocusEventArgs e)
        {
            if (e.VisualElement.Equals(passwordEntry))
            {
                passwordLayout.HasError = false;
            }
            if (e.VisualElement.Equals(usernameEntry))
            {
                usernameLayout.HasError = false;
            }
        }
        private async void login_Clicked(object sender, EventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(usernameEntry.Text) && string.IsNullOrEmpty(passwordEntry.Text))
                {
                    passwordLayout.HasError = usernameLayout.HasError = true;
                    return;
                }
                if (passwordLayout.HasError || usernameLayout.HasError) return;
                HttpClient httpClient = new HttpClient();
                MultipartFormDataContent form = new MultipartFormDataContent();

                form.Add(new StringContent(usernameEntry.Text), "username");
                form.Add(new StringContent(passwordEntry.Text), "password");
                HttpResponseMessage response = await httpClient.PostAsync("https://uxcandy.com/~shapoval/test-task-backend/v2/login/?developer=AndriiShvets", form);
                if (response.IsSuccessStatusCode)
                {
                    string json = await response.Content.ReadAsStringAsync();
                    var login = JsonConvert.DeserializeObject<Login>(json);
                    if(login.status == "error")
                    {
                        await DisplayAlert("Помилка", login.message.password, "Ok");
                        return;
                    }
                    else
                    {
                        Settings.Token = login.message.token;
                    }
                }
                MessagingCenter.Send(this, "RefreshView");
                await PopupNavigation.Instance.PopAsync();

            }
            catch (Exception ex)
            {

            }
        }
    }
}