﻿using Plugin.Settings;
using Plugin.Settings.Abstractions;

namespace TestTaskBeeJee.Services
{
    /// <summary>
    /// This is the Settings static class that can be used in your Core solution or in any
    /// of your client applications. All settings are laid out the same exact way with getters
    /// and setters. 
    /// </summary>
    public static class Settings
    {
        private static ISettings AppSettings
        {
            get
            {
                return CrossSettings.Current;
            }
        }

        #region Setting Constants

        private const string SettingsKey = "settings_key";
        private static readonly string SettingsDefault = string.Empty;

        #endregion


        public static string GeneralSettings
        {
            get
            {
                return AppSettings.GetValueOrDefault(SettingsKey, SettingsDefault);
            }
            set
            {
                AppSettings.AddOrUpdateValue(SettingsKey, value);
            }
        }
        public static string SelectSortField
        {
            get
            {
                return AppSettings.GetValueOrDefault("SelectSortField", "username");
            }
            set
            {
                AppSettings.AddOrUpdateValue("SelectSortField", value);
            }
        }
        public static string SelectSortDirection
        {
            get
            {
                return AppSettings.GetValueOrDefault("SelectSortDirection", "asc");
            }
            set
            {
                AppSettings.AddOrUpdateValue("SelectSortDirection", value);
            }
        }
        public static string Token
        {
            get
            {
                return AppSettings.GetValueOrDefault("Token", "");
            }
            set
            {
                AppSettings.AddOrUpdateValue("Token", value);
            }
        }
    }
}
