﻿using System.Collections.Generic;

namespace TestTaskBeeJee.Models
{
    public class Task
    {
        public int id { get; set; }
        public string username { get; set; }
        public string email { get; set; }
        public string text { get; set; }
        public int status { get; set; }
        public string statusText { get; set; }
    }

    public class Message
    {
        public List<Task> tasks { get; set; }
        public string total_task_count { get; set; }
    }

    public class Root
    {
        public string status { get; set; }
        public Message message { get; set; }
    }

    public class Login
    {
        public string status { get; set; }
        public MessageLogin message { get; set; }
    }
    public class MessageLogin
    {
        public string token { get; set; }
        public string password { get; set; }
    }

    public class Section2Model
    {
        public string Title { get; set; }
        public string Text { get; set; }
        public string Days { get; set; }
        public string Url { get; set; }
    }

    public class SectionModel
    {
        public string SectionType { get; set; }
        public Section2Model Section { get; set; }
    }

    public class StoryModel
    {
        public string Id { get; set; }
        public string Title { get; set; }
        public string Note { get; set; }
        public string ImageUrl { get; set; }
        public string PreviewImageUrl { get; set; }
        public List<SectionModel> Sections { get; set; }
    }
    public class StoryVMModel
    {
        public string DayNumber { get; set; }
        public string Title { get; set; }
        public string MainText { get; set; }
        public string Images { get; set; }
        public string QuoteText { get; set; }
        public bool IsDay { get; set; } = false;
    }
}


