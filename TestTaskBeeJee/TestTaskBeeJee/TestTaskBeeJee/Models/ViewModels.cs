﻿namespace TestTaskBeeJee.ViewModels
{
    public class PageNumberVM
    {
        public int pageNumber { get; set; }
        public PageNumberVM() { }
        public PageNumberVM(int page)
        {
            pageNumber = page;
        }

    }
    public class SortVM
    {
        public string sortDisplay { get; set; }
        public string sortNameApi { get; set; }
        public SortVM() { }
        public SortVM(string displayName, string sortApiName)
        {
            sortDisplay = displayName;
            sortNameApi = sortApiName;
        }
    }

}
