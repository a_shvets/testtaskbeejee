﻿using TestTaskBeeJee.Pages;
using Xamarin.Forms;

namespace TestTaskBeeJee
{
    public partial class App : Application
    {
        public App()
        {
            Syncfusion.Licensing.SyncfusionLicenseProvider.RegisterLicense("NDA0ODgxQDMxMzgyZTM0MmUzMGZ5M1FhYVRQbmpmR2FLb2djbFpxUlljdkVWdHh0VlZJdTVlYk9mNllzajQ9");
            InitializeComponent();
            MainPage = new NavigationPage(new AllTaskPage());
        }

        protected override void OnStart()
        {
        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }
    }
}
